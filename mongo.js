// What is MongoDB Atlas?
	// MongoDB Atlas is a MongoDB database in the cloud. It is the cloud service of MongoDB, the leading noSQL database.

// What is Robo3T?
	// Robo3T is an application which allows the use of GUI to manipulate MongoDB databases. The advantage of Robo3T in just using Atlas is that Robo3T is a local application.

// What is Shell?
	// Shell is an interface in MongoDB that allows us to input commands and perform CRUD operations in our database.

// CRUD Operations
	// Create, Read, Update, and Delete

	// Create - inserting documents
		// insertOne
	/*
		db.collection.insertOne({
			"fieldA" : "value A",
			"fieldB" : "value B"
		}) - allows us to insert a document into a collection
	*/

	db.users.insertOne({

		"firstName" : "Jean",
		"lastName" : "Smith",
		"age" : 24,
		"contact" : {
			"phone" : "09123456789",
			"email" : "js@mail.com"
		},
		"courses" : [
			"CSS", "JavaScript", "ReactJS"
		],
		"department" : "none"
	});

	// InserMany({})
		/*
			Syntax:
			db.collections.insertMany([
				{objectA}, {objectB}
			])
		*/


	db.users.insertMany([
		{
			"firstName" : "Stephen",
			"lastName" : "Hawking",
			"age" : 76,
			"contact" : {
				"phone" : "09123456789",
				"email" : "stephenhawking@mail.com"
		},
			"courses" : [
				"Python", "PHP", "React"
		],
			"department" : "none"
		},
		{
			"firstName" : "Neil",
			"lastName" : "Armstrong",
			"age" : 82,
			"contact" : {
				"phone" : "09987654321",
				"email" : "armstrong@mail.com"
		},
			"courses" : [
				"Laravel", "Sass", "Springboot"
		],
			"department" : "none"
		},
	])

	// Read - find documents
	/*
			Syntax:
			db.collections.find({});
				// will return All documents
			db.collections.findOne({});
				//will return the first document
			db.collections.find({"criteria" : "value"});
				//will return All documents that matches the criteria in the collection
			db.collections.findOne({"criteria" : "value"});
				//will return the first document that matches the criteria in the collection
			db.collection.find({"fieldA" : "valueA", "fieldB" : "valueB"})
	*/

	db.users.find()

	db.users.findOne({})

	db.users.find({"firstName" : "Neil"})

	db.users.findOne({"firstName" : "Kate"})

	db.users.find({"lastName" : "Armstrong", "age" : 82})

	// Update - updates documents
	/*
		Syntax:
		db.collection.updateOne({"criteria" : "Value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
			//to update the first item that matches the cirteria

		db.collection.updateOne({}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
			//to update the first item in the collection

		db.collection.updateMany({"criteria" : "Value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
			//to update all items that matches the cirteria

		db.collection.updateMany({}, {$set:{"fieldToBeUpdated" : "updatedValue"}})
			//to update all items in the collection

	*/

	db.users.updateOne({"firstName" : "John"}, {$set: {"lastName" : "Gates"}})

	db.users.updateOne({}, {$set:{"emergencyContact" : "father"}})

	db.users.updateMany({"department" : "none"}, {$set:{"department" : "Tech"}})

	db.users.updateMany({"department" : "Tech"}, {$rename:{"dept" : "Tech"}})

	db.users.updateMany({}, {$set:{"emergencyContact" : "mother"}})

	db.users.updateOne({}, {$rename: {"dept" : "department"}})

	// Delete - to delete documents
	/*
		Syntax:

			db.collection.deleteOne({"criteria" : "value"})
				//delete the first item that mathes the criteria

			db.collection.deleteMany({"criteria" : "value"})
				//delete ALL items that mathe the criteria

			db.collection.deleteOne({})
				//delete the first item in the collection

			db.collection.deleteMany({})
				//delete ALL the items in a collection
	*/

	db.users.deleteOne({"firstName" : "Neil"})

	db.users.deleteOne({})

	db.users.deleteMany({"lastName" : "Middleton"})

	db.users.deleteMany({})

	db.users.insertOne({
		"ame" : "Kim Jin",
		"bDay" : "November 10, 1775",
		"age" : 25,
	})

	db.users.aggraegate($unset: "age")


	db.users.insertMany([
		{
			"firstName" : "Frodo",
			"lastName" : "Baggins",
			"email" : "FrodoB@mail.com",
			"password" : "theOneRing",
			"isAdmin" : false
		},
		{
			"firstName" : "Samwise",
			"lastName" : "Gamgee",
			"email" : "Shiraq@mail.com"
			"password" : "Potatos",
			"isAdmin" : false
		},
		{
			"firstName" : "Pippin",
			"lastName" : "Took",
			"email" : "Took@mail.com",
			"password" : "2ndBreakfast",
			"isAdmin" : false
		},
		{
			"firstName" : "Merry",
			"lastName" : "Brandybuck",
			"email" : "MerryB@mail.com",
			"password" : "elevensies",
			"isAdmin" : false
		},
		{
			"firstName" : "Gandalf",
			"lastName" : "White",
			"email" : "GWhiteB@mail.com",
			"password" : "youShallNotPass",
			"isAdmin" : false
		},
	])
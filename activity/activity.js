	db.users.insertMany([
		{
			"firstName" : "Frodo",
			"lastName" : "Baggins",
			"email" : "FrodoB@mail.com",
			"password" : "theOneRing",
			"isAdmin" : false
		},
		{
			"firstName" : "Samwise",
			"lastName" : "Gamgee",
			"email" : "Shiraq@mail.com",
			"password" : "Potatos",
			"isAdmin" : false
		},
		{
			"firstName" : "Pippin",
			"lastName" : "Took",
			"email" : "Took@mail.com",
			"password" : "2ndBreakfast",
			"isAdmin" : false
		},
		{
			"firstName" : "Merry",
			"lastName" : "Brandybuck",
			"email" : "MerryB@mail.com",
			"password" : "elevensies",
			"isAdmin" : false
		},
		{
			"firstName" : "Gandalf",
			"lastName" : "White",
			"email" : "GWhiteB@mail.com",
			"password" : "youShallNotPass",
			"isAdmin" : false
		},
	])

	db.courses.insertMany([
		{
			"name" : "Fellowship Assembly",
			"price" : 500,
			"isActive" : false
		},
		{
			"name" : "Walking to Mordor",
			"price" : 9500,
			"isActive" : false
		},
		{
			"name" : "Cooking with Orcs",
			"price" : 1000,
			"isActive" : false
		},
	])


	db.users.find({"isAdmin" : false})

	db.users.updateOne({}, {$set:{"isAdmin" : true}})

	db.courses.updateOne({"name" : "Cooking with Orcs"}, {$set: {"isActive" : true}})

	db.courses.deleteMany({"isActive" : false})